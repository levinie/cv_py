import cv2
import numpy as np

# img_path = "/home/l/PycharmProjects/cv_py/pic/beauty.jpg"
img_path = "../pic/beauty.jpg"

img = cv2.imread(img_path)

print img
print "======= property ======="
print "ndim:", img.ndim
print "shape:", img.shape
print "size:", img.size
print "dtype:", img.dtype
print "itemsize:", img.itemsize
# print "data:", img.data
print "====== create method ======"
print img.reshape(1200, 2418)
print np.arange(15)
print np.zeros((2, 3), np.uint8)
print np.ones((2, 3), np.uint8)
print np.empty((2, 3), np.uint8)
print np.arange(10, 35, 5)
print "====== operation ======"
print img > 2
